
import 'package:tezart/tezart.dart';

import '../shared.dart';
import '../tezos_service.dart';
Future<void> main() async {

  /// Generate keystore from mnemonic
  var keystore = Keystore.fromMnemonic(Shared.dukeMnemonic);

  // Sample output of keystore created from mnemonic

  reveal(keystore);
  print(checkReveal(keystore));
  call(keystore);
  print("cbon");



}