
import 'package:tezart/tezart.dart';

import '../shared.dart';



Future<void> reveal(Keystore keyStore) async {
  final client = TezartClient(Shared.rpcNode);
  final operation = client.revealKeyOperation(keyStore);
  try {
    await operation.executeAndMonitor();
  } catch (e) {
    rethrow;
  }
}

Future<bool> checkReveal(Keystore keyStore) async {
  final client = TezartClient(Shared.rpcNode);
  return await client.isKeyRevealed(keyStore.address);
}

Future<void> call(Keystore keystore) async {

  final rpcInterface = RpcInterface(Shared.rpcNode);
  final contract = Contract(
    contractAddress: Shared.contractAddress,
    rpcInterface: rpcInterface,
  );

  final operations = await contract.callOperation(
    entrypoint: "add_user",
    params: '',
    source: keystore,

  );
  await operations.execute();
}





